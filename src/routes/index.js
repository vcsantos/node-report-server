const express =  require('express');

const araujoRouter = require('./araujo.routes');

const routes = express.Router();

routes.use('/Araujo', araujoRouter);

module.exports= routes;