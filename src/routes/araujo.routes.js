const JasperReportModel = require('../models/JasperReportModel');
const express =  require('express');

const araujoRouter = express.Router();

const contentJson = [
    {
        "hour": "06:00",
        "dock": "1",
        "supplier": "NESTLE BRASIL",
        "transptype": "Carreta",
        "carrier": "NESTLE BRASIL",
        "vol": 2574,
        "sku": 2,
        "fiscal": "0202000103333"
    },
    {
        "hour": "07:00",
        "dock": "2",
        "supplier": "ATIVA",
        "transptype": "Truck",
        "carrier": "ATIVA",
        "vol": 0,
        "sku": "",
        "fiscal": ""
    }
];

araujoRouter.post('/report/:nameReport', async (req, res) => {
    const { nameReport } = req.params;

    let pdf = await JasperReportModel.generatePdfReport({
        report: nameReport,
        data: {},
        dataset: contentJson
    });

    res.set({
        'Content-type': 'application/pdf',
        'Content-Length': pdf.length
    });

    res.setHeader("Content-disposition", " filename=" + nameReport + '.pdf');

    return res.send(pdf);

});


module.exports= araujoRouter;