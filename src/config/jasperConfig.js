 const path = require('path');

 module.exports = {
    path: path.resolve('lib','jasperreports-6.1.0'),
    reports: {
        dailyAppointment: {
            jasper: path.resolve('reports','teste.jasper'),
            conn: 'in_memory_json'
        },
        fixedWindow: {
            jasper: path.resolve('reports','teste.jasper'),
            conn: 'in_memory_json'
        }
    }
}