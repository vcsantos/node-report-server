const jasperConfig = require('../config/jasperConfig');
const jasper = require('node-jasper')(jasperConfig);

class JasperReportModel {
    async generatePdfReport(reportObject){
        const report = {
            report: reportObject.report,
            data: reportObject.data,
            dataset: reportObject.dataset,
            conn: reportObject.dataset
        }

        const pdfFile = await jasper.pdf(report);

        return pdfFile;
    }

}


module.exports= new JasperReportModel;